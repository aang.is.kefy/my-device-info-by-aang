import {NativeModules} from 'react-native';
const {DeviceInfoModule: DeviceInfo} = NativeModules;

export default DeviceInfo;
