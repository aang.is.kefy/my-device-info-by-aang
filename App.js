import React, {useEffect, useState} from 'react';
import {StyleSheet, StatusBar, View, Text} from 'react-native';
import DeviceInfo from './device-info/index';

export default function App() {
  const [deviceName, setDeviceName] = useState('This should be device name');
  const [operatingSystem, setOperatingSystem] = useState(
    'This should be operating system',
  );
  const [internalStorageUsed, setInternalStorageUsed] = useState(null);
  useEffect(() => {
    try {
      setDeviceName(DeviceInfo.getDeviceModel());
      setOperatingSystem(DeviceInfo.getOperatingSystem());
      setInternalStorageUsed(DeviceInfo.getDeviceStorage());
    } catch (error) {}
  }, []);
  return (
    <React.Fragment>
      <View style={styles.container}>
        <StatusBar barStyle="light-content" backgroundColor="blue" />
        <View style={styles.toolbar}>
          <Text style={styles.header}>My Device Info</Text>
        </View>
        <View style={styles.content}>
          <Text style={styles.largeText}>{deviceName.toUpperCase()}</Text>
          <Text style={styles.mediumText}>{operatingSystem.toUpperCase()}</Text>
          <View style={styles.spacer} />
          <Text style={styles.largeText}>Internal Storage</Text>
          {internalStorageUsed && (
            <Text style={styles.mediumText}>{internalStorageUsed}% Used</Text>
          )}
          <View style={styles.spacer} />
        </View>
        <View style={styles.footer}>
          <Text>Made with ♥ by Aang Kunaefi</Text>
        </View>
      </View>
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  toolbar: {
    alignItems: 'center',
    backgroundColor: 'blue',
    padding: 15,
    elevation: 5,
  },
  header: {
    color: 'white',
    fontWeight: '700',
    fontSize: 18,
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  largeText: {
    fontSize: 22,
    fontWeight: '700',
    textAlign: 'center',
  },
  mediumText: {
    fontSize: 16,
  },
  spacer: {
    height: 50,
  },
  footer: {
    alignItems: 'center',
    padding: 15,
  },
});
