package com.mydeviceinfo;

import android.os.Build;
import android.os.StatFs;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

@SuppressWarnings("ALL")
public class DeviceInfoModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;

    DeviceInfoModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
    }

    @NonNull
    @Override
    public String getName() {
        return "DeviceInfoModule";
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public String getDeviceModel() {
        return String.format("%s %s", Build.MANUFACTURER, Build.MODEL);
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public String getOperatingSystem() {
        return String.format("Android %s", Build.VERSION.RELEASE);
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public String getDeviceStorage() {
        StatFs stat = new StatFs(reactContext.getFilesDir().getAbsolutePath());
        long bytesTotal = stat.getBlockSizeLong() * stat.getBlockCountLong();
        long bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
        double megaAvailable = bytesAvailable / (1024 * 1024);
        double megaTotal = bytesTotal / (1024 * 1024);
        return String.format("%s", (int) ((megaTotal - megaAvailable) / megaTotal * 100.00));
    }

}